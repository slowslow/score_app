from score.settings.base import *

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG = True

ALLOWED_HOSTS = ["*"]

SECRET_KEY = '9d7k_f237q#ad(sh^w8vc96eh=y6###4#uiceox(ig4g*gcp+3'

# https://docs.djangoproject.com/en/2.1/topics/email/#file-backend - use this for testing
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
