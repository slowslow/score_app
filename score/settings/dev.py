from .base import *

ALLOWED_HOSTS = ["*"]
DEBUG = True
SECRET_KEY = 'ira8@=ic7&9!moz7j1!1cj5z#pz^zo&6!bxhh%fel8cpogpsjl'
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'score',
        'USER': 'score_user',
        'PASSWORD': 'YOUR_PASSWORD',
        'HOST': 'localhost',
        'PORT': '',
    }
}
