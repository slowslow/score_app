from django.conf.urls import url

from . import views

app_name = 'profiles'
urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^delete_account/$', views.delete_account, name='delete_account'),
    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^info/$', views.user_profile, name='user_profile'),
]
