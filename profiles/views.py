from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from .forms import SignUpForm
from .tokens import account_activation_token

from django.contrib.auth.models import User
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth import login, logout, authenticate

from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from django.contrib.auth.decorators import login_required

import os

from django.contrib.auth.forms import AuthenticationForm


@login_required
def user_profile(request):
    user = User.objects.get(id=request.user.id)
    if request.method == 'GET':
        return render(
            request,
            'profiles/user_profile.html',
            {'user': user, }, )
    elif request.method == 'POST':
        user.username  = request.POST['userName']
        user.email = request.POST['userEmail']
        user.save()
        return HttpResponseRedirect(reverse('profiles:user_profile'))


@login_required
def delete_account(request):
    user = User.objects.get(id=request.user.id)
    user.delete()
    return HttpResponseRedirect(reverse('profiles:signup'))

@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('profiles:signin'))


def account_activation_sent(request):
    return render(request, 'profiles/account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        #login(request, user)
        return HttpResponseRedirect(reverse('profiles:signin'))
    else:
        return render(request, 'account_activation_invalid.html')


def signup(request):
    if request.method == 'POST':
        if request.user.is_active:
            logout(request)
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your MySite Account'
            message = render_to_string('profiles/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)

            dir = os.path.dirname(__file__)
            log_file = os.path.join(dir, '../logs/tokens.log')
            with open(log_file, 'a') as the_file:
                the_file.write(message)

            return HttpResponseRedirect(reverse('profiles:account_activation_sent'))
    else:
        form = SignUpForm()
    return render(request, 'profiles/register.html', {'form': form})


def signin(request):
    if request.method == 'GET':
        if request.user.is_active:
            return HttpResponseRedirect(reverse('dataset:upload'))
        else:
            form = AuthenticationForm()
            return render(request,'profiles/signin.html', {'form': form} )
    elif request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('dataset:upload'))
        else:
            form.add_error(None, "Wrong")
            return render(request,'profiles/signin.html', {'form': form} )
