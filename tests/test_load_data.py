#!/usr/bin/env python3

from time import sleep
import unittest
from selenium import webdriver
from selenium.common.exceptions import *


class LoadDataTest(unittest.TestCase):

    ROOT_URL = "http://localhost:8000"
    user_name = "example_user"
    email = "example@email.com"
    password = "mi756tsl"

    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        # options.add_argument('headless')
        self.driver = webdriver.Chrome(options=options)
        self.driver.maximize_window()

    def test_1_login(self):
        driver = self.driver
        driver.get(self.ROOT_URL + "/profile/signin/")
        driver.find_element_by_name("username").send_keys(self.user_name)
        driver.find_element_by_name("password").send_keys(self.password)
        driver.find_element_by_css_selector(
                '#page-top > div > div > div.card-body > form > div:nth-child(4) > input[type="submit"]:nth-child(1)').click()
        assert self.ROOT_URL + "/dataset" in driver.current_url

    def test_2_load_data(self):
        driver = self.driver
        try:
            driver.find_element_by_name('file').send_keys('/home/kombisu/score_test/data/titanic.csv')
        except NoSuchElementException:
            self.assertTrue(False, 'no file input field')
        driver.find_element_by_css_selector('#page-top > div > div > div > form > div > div:nth-child(2) > button').click()
        driver.find_element_by_css_selector('#page-top > div > div > div > form > button').click()

    def test_3_check_data(self):
        driver = self.driver
        driver.get(self.ROOT_URL + "/dataset")
        # getting dataset id
        driver.find_element_by_css_selector("#page-top > div > div > div > div.table-responsive > table > tbody > tr:nth-child(2) > td:nth-child(1) > a").click()
        driver.find_element_by_css_selector("#page-top > div > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(1) > a").click()
        sleep(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(1)
        driver.execute_script("window.scrollTo(document.body.scrollHeight, 0);")
        input_field = driver.find_element_by_name('field_name')
        input_field.clear()
        input_field.send_keys('Gender')
        driver.find_element_by_class_name('btn').click()
        driver.find_element_by_class_name('btn').click()
        assert self.ROOT_URL + "/dataset" in driver.current_url

    def test_4_build_model(self):
        driver = self.driver
        driver.find_element_by_class_name('fa-wrench').click()
        driver.find_element_by_css_selector('#page-top > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(3) > a:nth-child(1)').click()

    def test_5_test_form(self):
        driver = self.driver
        driver.get(self.ROOT_URL + "/model")
        driver.find_element_by_css_selector('#page-top > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(3) > a:nth-child(2)').click()
        driver.find_element_by_name('PassengerId').send_keys('1')
        driver.find_element_by_name('Pclass').send_keys('1')
        driver.find_element_by_name('Age').send_keys('1')
        driver.find_element_by_name('SibSp').send_keys('1')
        driver.find_element_by_name('Parch').send_keys('1')
        driver.find_element_by_name('Fare').send_keys('1')
        el = driver.find_element_by_name('Embarked')
        for option in el.find_elements_by_tag_name('option'):
            if option.text == 'C':
                option.click()  # select() in earlier versions of webdriver
                break
        driver.find_element_by_class_name('btn-primary').click()
        assert "your result is" in driver.find_element_by_class_name('card-body').text

    def test_6_delete_data(self):
        driver = self.driver
        driver.get(self.ROOT_URL + '/dataset')
        driver.find_element_by_css_selector(
                '#page-top > div > div > div > div.table-responsive > table > tbody > tr:nth-child(2) > td:nth-child(4) > a:nth-child(2)').click()

    @classmethod
    def tearDownClass(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
