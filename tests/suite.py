#!/usr/bin/env python3

import unittest
from test_create_user import *
from test_load_data import *
#from test_build_model import *
from test_user_profile import *
from test_delete_user import *

# get all tests
create_user = unittest.TestLoader().loadTestsFromTestCase(CreateUserTest)
load_data = unittest.TestLoader().loadTestsFromTestCase(LoadDataTest)
#build_model = unittest.TestLoader().loadTestsFromTestCase(BuildModelTest)
check_user = unittest.TestLoader().loadTestsFromTestCase(CheckUserTest)
delete_user = unittest.TestLoader().loadTestsFromTestCase(DeleteUserTest)

# create a test suite
test_suite = unittest.TestSuite([create_user, load_data, check_user, delete_user])

# run the suite
unittest.TextTestRunner(verbosity=2).run(test_suite)
