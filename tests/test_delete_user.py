#!/usr/bin/env python3

import unittest
from selenium import webdriver


class DeleteUserTest(unittest.TestCase):

    ROOT_URL = "http://localhost:8000"
    user_name = "example_user"
    email = "example@email.com"
    password = "mi756tsl"

    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        # options.add_argument('headless')
        self.driver = webdriver.Chrome(options=options)
        self.driver.maximize_window()

    def test_1_login(self):
        driver = self.driver
        driver.get(self.ROOT_URL + "/profile/signin/")
        driver.find_element_by_name("username").send_keys(self.user_name)
        driver.find_element_by_name("password").send_keys(self.password)
        driver.find_element_by_css_selector(
                '#page-top > div > div > div.card-body > form > div:nth-child(4) > input[type="submit"]:nth-child(1)').click()

    def test_2_delete_user(self):
        driver = self.driver
        driver.get("http://localhost:8000/profile/info/")
        driver.find_element_by_xpath('//*[@id="page-top"]/div/div/div/div/div/a').click()

    @classmethod
    def tearDownClass(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
