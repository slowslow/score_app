#!/bin/bash

while getopts ":e:p:u:" opt; do
    case $opt in
        e) email="$OPTARG"
        ;;
        p) password="$OPTARG"
        ;;
        u) username="$OPTARG"
        ;;
        \?) echo "Invalid option -$OPTARG" >&2
        ;;
    esac
done

cd ~/score_app
source ~/.virtualenvs/score_app-bV8oZDVJ/bin/activate
export DJANGO_SETTINGS_MODULE="score.settings.test"
echo "from django.contrib.auth.models import User; User.objects.create_superuser('${username}', '${email}', '${password}')" | python manage.py shell
