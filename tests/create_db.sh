#!/bin/bash

sudo -u postgres createdb score
sudo -u postgres psql -c "CREATE ROLE score_user PASSWORD 'YOUR_PASSWORD'"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE score TO score_user;"
sudo -u postgres psql -c "ALTER ROLE score_user WITH LOGIN;"
# rm -f upload/*
# rm -f models/*
# find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
# find . -path "*/migrations/*.pyc"  -delete
# export DJANGO_SETTINGS_MODULE="score.settings.test"
# python manage.py makemigrations
# python manage.py migrate
# python manage.py runserver 0.0.0.0:8000 &
