import pandas as pd
import json
import logging
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from .settings import *
from .files import get_size
from .models import Dataset, Field
from .forms import UploadFileForm
from storages.backends.dropbox import DropBoxStorage


logger = logging.getLogger(__name__)


# Представление, загружающее dataset на диск.
@login_required
def upload(request, **kwargs):
    if request.user.is_superuser:
        datasets = Dataset.objects.all()
    else:
        datasets = Dataset.objects.filter(user=request.user)

    if request.method == 'GET':
        form = UploadFileForm()
        return render(
            request,
            'dataset/upload.html', {
                'form': form,
                'dataset': datasets}, )

    elif request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if not request.user.is_superuser:
            if Dataset.objects.filter(user=request.user).exists():
                data = {'is_valid': False, 'error': "You alredy have dataset"}
                return JsonResponse(data)
        if form.is_valid():
            try:
                form_field = form.save(commit=False)
                # check if another file exist for non superuser
                form_field.user = request.user
                form_field.save()
                data = {'is_valid': True, 'name': form_field.file.name, 'url': form_field.file.url, 'id': form_field.id}
            except Exception as e:
                # Dataset.objects.last().delete()
                data = {'is_valid': False, 'error': str(e)}
            return JsonResponse(data)
        else:
            data = {'is_valid': False, 'error': form.errors}
            return JsonResponse(data)


# Представление с выбором целевой переменной и статистикой.
@login_required
def edit(request, dataset_id):
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=dataset_id, user=request.user)
        else:
            dataset = Dataset.objects.get(pk=dataset_id)
    except Exception:
        return HttpResponseRedirect(reverse('dataset:upload'))

    # Use "nrows = 0" in read_csv method to read 0 rows
    # if isinstance(dataset.file.storage, DropBoxStorage)
    data_frame = pd.read_csv(dataset.file.url, nrows=0)
    for name in data_frame.columns:
        if not Field.objects.filter(data_file=dataset, name=name).exists():
            Field(data_file=dataset, name=name, field_type=str(data_frame[name].dtype)).save()

    fields = Field.objects.filter(data_file_id=dataset_id)
    description = json.loads(dataset.description)

    bool_fields = []
    for name in description.keys():
        if description[name]["dtype"] == "boolean":
            bool_fields.append(name)

    if request.method == 'GET':
        return render(
            request,
            'dataset/edit.html', {
                'fields': fields,
                'bool_fields': bool_fields,
                'dataset': dataset})

    elif request.method == 'POST':
        Dataset.objects.filter(
            pk=dataset_id).update(name=request.POST['file_name'])
        for field in fields:
            if field.name == request.POST['choice']:
                Field.objects.filter(
                    data_file=dataset,
                    name=field.name).update(is_y=True)
            else:
                Field.objects.filter(
                    data_file=dataset,
                    name=field.name).update(is_y=False)
        return HttpResponseRedirect(reverse('dataset:upload'))


@login_required
def edit_field(request, dataset_id, field_name):
    # read data file from disc
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=dataset_id, user=request.user)
        else:
            dataset = Dataset.objects.get(pk=dataset_id)
    except Exception:
        return HttpResponseRedirect(reverse('dataset:upload'))

    # get field from db
    field = get_object_or_404(Field, name=field_name, data_file_id=dataset_id)

    # get description
    desc = json.loads(dataset.description)

    if request.method == 'GET':
        return render(
            request,
            'dataset/edit_field.html', {
                'field': field,
                'desc': desc[field_name],
                'dataset_id': dataset_id})

    elif request.method == 'POST':
        # change name in dataset_field table
        field.name = request.POST.get('field_name')
        field.save(force_update=True)

        # change name in csv file
        data_frame = pd.read_csv(dataset.file.url)
        data_frame.columns = data_frame.columns.str.replace(
            field_name,
            request.POST.get('field_name'))

        dataset.update_file(data_frame)
        # change name in description file
        desc[field_name]["help_text"] = request.POST.get("help_text")
        if request.POST.get("max_value"):
            desc[field_name]["max_value"] = request.POST.get("max_value")
        if request.POST.get("min_value"):
            desc[field_name]["min_value"] = request.POST.get("min_value")
        desc[request.POST.get('field_name')] = desc.pop(field_name)

        # change description in dataset_dataset table
        dataset.description = json.dumps(desc, separators=(',', ':'))
        dataset.save()

        return HttpResponseRedirect(
            reverse(
                'dataset:edit',
                args=[dataset_id]))


# Delete dataset
@login_required
def delete_dataset(request, dataset_id):
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=dataset_id, user=request.user)
        else:
            dataset = Dataset.objects.get(pk=dataset_id)
    except Exception:
        return HttpResponseRedirect(reverse('dataset:upload'))

    dataset.file.delete()
    dataset.delete()
    for field in Field.objects.filter(data_file_id=dataset_id):
        field.delete()
    return HttpResponseRedirect(reverse('dataset:upload'))


# Hide field
@login_required
def hide_field(request):
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=request.GET.get('dataset_id'), user=request.user)
        else:
            dataset = Dataset.objects.get(pk=request.GET.get('dataset_id'))
    except Exception:
        return HttpResponse("Error {}".format(dataset_id))

    desc = json.loads(dataset.description)
    if desc[request.GET.get('field_id')]["active"] == "yes":
        desc[request.GET.get('field_id')]["active"] = "no"
    else:
        desc[request.GET.get('field_id')]["active"] = "yes"
    return HttpResponse("OK")
