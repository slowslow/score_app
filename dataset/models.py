import json
import pandas as pd
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from storages.backends.dropbox import DropBoxStorage
from django.conf import settings
from dropbox.files import WriteMode
from .validators import validate_file_extension, validate_file_size
from .preprocessing import *
from .settings import *


class Dataset(models.Model):
    name = models.CharField(max_length=30)
    row_count = models.IntegerField(default=0)
    num_fields = models.IntegerField(default=0)
    file = models.FileField(upload_to=CSV_DIR, validators=[
                            validate_file_extension, validate_file_size])
    status = models.CharField(max_length=30, default="disable")
    label_encode = models.BinaryField()
    description = models.TextField(default='')
    cleaned = models.BooleanField(default=False)
    user = models.ForeignKey(User, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.file.name

    # метод возвращает y для dataset
    def get_y_name(self):
        return Field.objects.get(data_file=self, is_y=True).name

    # метод возвращает массив с именами x
    def get_x_names(self):
        names = []
        for field in Field.objects.filter(data_file=self, is_y=False):
            names.append(field.name)
        return names

    def update_file(self, data_frame):
        if isinstance(self.file.storage, DropBoxStorage):
            dbx = DropBoxStorage(settings.DROPBOX_OAUTH2_TOKEN)
            mode = WriteMode.overwrite
            name = self.file.file.name
            data = data_frame.to_csv(index=False)
            dbx.client.files_upload(bytes(data, 'utf-8'), name, mode)
        else:
            data_frame.to_csv(self.file.url, index=False)

@receiver(post_save, sender=Dataset)
def clean_data(instance, created, **kwargs):
    ''' очистка данных '''
    if created:
        data_frame = pd.read_csv(instance.file.url)
        drop_strings(data_frame)
        data_frame = drop_na(data_frame)
        instance.update_file(data_frame)
        instance.cleaned = True
        instance.save()


@receiver(post_save, sender=Dataset)
def add_name(instance, created, **kwargs):
    ''' добавление имени '''
    if created:
            data_frame = pd.read_csv(instance.file.url)
            instance.name = 'file' + str(instance.id)
            instance.row_count, instance.num_fields = data_frame.shape
            instance.save()


@receiver(post_save, sender=Dataset)
def add_description(instance, created, **kwargs):
    ''' добавление описания '''
    if created:
        data = pd.read_csv(instance.file.url)
        field_desc = {}
        for name in data.columns:
            field_desc[name] = {}

            # get statistic
            statistic = data[name].describe(percentiles=[]).to_json()
            field_desc[name]["statistic"] = json.loads(statistic)
            uniq_fields = list(data[name].unique())
            val_counts = data[name].value_counts().to_dict()
            field_desc[name]["active"] = "yes"
            if str(data[name].dtype) == 'object':
                field_desc[name]["type"] = "select"
                field_desc[name]["choices"] = uniq_fields
                field_desc[name]["history"] = list(val_counts.keys())
                field_desc[name]["values"] = list(val_counts.values())
            else:
                field_desc[name]["type"] = "number"
            if len(uniq_fields) == 2:
                field_desc[name]["dtype"] = "boolean"
            elif len(uniq_fields) > 2:
                field_desc[name]["dtype"] = "order"
        try:
            instance.description = json.dumps(field_desc, separators=(',', ':'))
        except Exception as e:
            print(field_desc)
            raise e
        instance.save()


class Field(models.Model):
    data_file = models.ForeignKey(Dataset, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    is_y = models.BooleanField(default=False)
    field_type = models.CharField(max_length=200)

    def __str__(self):
        return self.name
