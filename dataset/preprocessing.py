import os
import pandas as pd
import json


def drop_strings(data_frame):
    for name in data_frame.columns:
        if str(data_frame[name].dtype) == 'object' and data_frame[name].nunique() >= 20:
            data_frame.drop(name, axis=1, inplace=True)


# delete fields with missing values
def drop_na(data_frame):
    return data_frame.dropna()
