import os
from django.core.exceptions import ValidationError
from django.template.defaultfilters import filesizeformat
from .settings import *


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.csv', ]
    if not ext.lower() in valid_extensions:
        raise ValidationError(
            u'Unsupported file extension "%s". Should be ".csv"' % ext.lower())


def validate_file_size(value):
    content = value.file
    if content.size > int(MAX_UPLOAD_SIZE):
        raise ValidationError(
            ('Please keep filesize under %s. Current filesize %s') %
            (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(content._size)))
