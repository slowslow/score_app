from django.conf.urls import url

from . import views

app_name = 'dataset'
urlpatterns = [
    url(r'^$', views.upload, name='upload'),
    url(r'^(?P<dataset_id>[0-9]+)/delete_dataset', views.delete_dataset, name='delete_dataset'),
    url(r'^(?P<dataset_id>[0-9]+)/edit$', views.edit, name='edit'),
    url(r'^(?P<dataset_id>[0-9]+)/(?P<field_name>[\w\-]+)$', views.edit_field, name='edit_field'),
    url(r'^hide_field/$', views.hide_field, name='hide_field'),
]
