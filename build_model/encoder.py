from sklearn.preprocessing import LabelEncoder


def make_encoder(dataset):
    encoded = {}
    for name in dataset.loc[:, dataset.dtypes == object]:
        label = LabelEncoder()
        label.fit(dataset[name].drop_duplicates())
        encoded[name] = label
    return encoded


def encode(encoded, dataset):
    for name in dataset.loc[:, dataset.dtypes == object]:
        dataset[name] = encoded[name].transform(dataset[name])
    return dataset
