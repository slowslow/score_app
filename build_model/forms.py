from django import forms
import json


def get_form(data_desc, y_field):
    fields = json.loads(data_desc)
    file_handler = FieldHandler(fields, y_field)
    return type('DynaForm', (forms.Form,), file_handler.formfields)


class FieldHandler():
    formfields = {}

    def __init__(self, fields, y_field):
        for field in fields:
            if not field == y_field:
                options = self.get_options(fields[field])
                f = getattr(
                    self,
                    "create_field_for_" + fields[field]['type']
                )(fields[field], options)
                self.formfields[field] = f

    def get_options(self, field):
        options = {}
        options['help_text'] = field.get("help_text", None)
        options['required'] = bool(field.get("required", 1))
        return options

    def create_field_for_number(self, field, options):
        options['max_value'] = int(field.get("max_value", "999999999"))
        options['min_value'] = int(field.get("min_value", "0"))
        return forms.IntegerField(
            widget=forms.NumberInput(
                attrs={'class': 'form-control'}), **options)

    def create_field_for_select(self, field, options):
        options['choices'] = [(c, c) for c in field['choices']]
        return forms.ChoiceField(
            widget=forms.Select(
                attrs={'class': 'form-control'}), **options)
