from django.db import models
from dataset.models import Dataset
from django.contrib.auth.models import User


class DataModel(models.Model):
    name = models.CharField(max_length=30)
    params = models.TextField()
    dataset = models.ForeignKey(Dataset, on_delete=models.CASCADE)
    file_model = models.BinaryField()
    metrics = models.TextField()
    user = models.ForeignKey(User, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Result(models.Model):
    data_model = models.ForeignKey(DataModel, on_delete=models.CASCADE)
    description = models.TextField(default='')
    pub_date = models.DateTimeField('date published')
    user = models.ForeignKey(User, default=1, on_delete=models.CASCADE)
    choice = models.TextField(default='')