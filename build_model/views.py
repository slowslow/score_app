import os
import json
import ast
import pickle
import pandas as pd
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.utils import timezone
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import *
from .models import DataModel, Result
from dataset.models import Dataset, Field
from .forms import get_form
from .encoder import *



@login_required
def all(request):
    if request.user.is_superuser:
        models = DataModel.objects.all()
    else:
        models = DataModel.objects.filter(user=request.user)
    metrics = {}
    for model in models:
        metrics[model.id] = ast.literal_eval(model.metrics)
    return render(
        request,
        'build_model/all.html',
        {'models': models, 'metrics': metrics})


@login_required
def build(request, dataset_id):
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=dataset_id, user=request.user)
        else:
            dataset = Dataset.objects.get(pk=dataset_id)
    except Exception as e:
        return HttpResponseRedirect(reverse('dataset:upload'))

    if not Field.objects.filter(data_file_id=dataset_id, is_y=True).exists():
        return HttpResponseRedirect(reverse('dataset:edit', args=(dataset_id,)))

    if request.method == 'GET':

        # reading CSV file
        data = pd.read_csv(dataset.file.url)

        encoded = make_encoder(data)
        if not dataset.label_encode:
            dataset.label_encode = pickle.dumps(encoded)
            dataset.save()
            # label_encode.close()

        # move encoder part in dataset signals
        encode(encoded, data)
        y_name = Dataset.objects.get(pk=dataset_id).get_y_name()
        X_names = Dataset.objects.get(pk=dataset_id).get_x_names()
        y = data[y_name]
        X = data[X_names]
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.8, random_state=241)

        clf = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial')
        models = [clf]
        for model in models:
            model.fit(X_train, y_train)
            y_pred = model.predict(X_test)

            # вычисляем метрики качества
            metrics = {}
            metrics["model_accuracy"] = round(accuracy_score(y_test, y_pred), 2)
            metrics["model_precision"] = round(precision_score(y_test, y_pred), 2)
            metrics["model_recall"] = round(recall_score(y_test, y_pred), 2)
            metrics["model_f1"] = round(f1_score(y_test, y_pred), 2)

            fid = pickle.dumps(model)
            params = json.dumps(model.get_params())
            try:
                DataModel(
                    file_model=fid,
                    dataset=dataset,
                    params=params,
                    metrics=metrics,
                    user=request.user).save()
            except Exception as e:
                print(e)
            # fid.close
        return HttpResponseRedirect(reverse('build_model:all'))


@login_required
def delete_model(request, model_id):
    try:
        if not request.user.is_superuser:
            model = DataModel.objects.get(pk=model_id, user=request.user)
        else:
            model = DataModel.objects.get(pk=model_id)
    except Exception:
        return HttpResponseRedirect(reverse('build_model:all'))
    model.delete()
    return HttpResponseRedirect(reverse('build_model:all'))


@login_required
def info(request, model_id):
    try:
        if not request.user.is_superuser:
            model = DataModel.objects.get(pk=model_id, user=request.user)
        else:
            model = DataModel.objects.get(pk=model_id)
    except Exception:
        return HttpResponseRedirect(reverse('build_model:all'))
    metrics = {}
    metrics[model.id] = ast.literal_eval(model.metrics)
    return render(
        request,
        'build_model/info.html',
        {'model': model, 'metrics': metrics})


# Представление, которое строит форму
@login_required
def test(request, model_id):
    try:
        if not request.user.is_superuser:
            model = DataModel.objects.get(pk=model_id, user=request.user)
        else:
            model = DataModel.objects.get(pk=model_id)
    except Exception:
        return HttpResponseRedirect(reverse('build_model:all'))
    if model:
        dataset = Dataset.objects.get(pk=model.dataset_id)
        form_class = get_form(
            dataset.description,
            dataset.get_y_name())
        if request.method == 'POST':
            form = form_class(request.POST)
            if form.is_valid():
                data = {}
                for field, value in form.cleaned_data.items():
                    data[field] = [value]
                user_data = pd.DataFrame(data)
                # загружаем файл с кодированными значениями
                encoded_labels = pickle.loads(dataset.label_encode)
                file_model = pickle.loads(model.file_model)
                encode(encoded_labels, user_data)
                y_name = dataset.get_y_name()
                answer = file_model.predict(user_data)

                result = Result(data_model=model, description=json.dumps(data), user=request.user, pub_date=timezone.now(), choice=answer)
                result.save()

                return render(
                    request,
                    "build_model/answ.html",
                    {'answ': answer, 'description': result.description})
        else:
            form = form_class()
        return render(request, "build_model/profile.html", {'form': form, })
    else:
        return render(request, "build_model/profile.html")


@login_required
def results(request):
    if request.user.is_superuser:
        results = Result.objects.all()
    else:
        results = Result.objects.filter(user=request.user)

    for result in Result.objects.all():
        result.description = (json.loads(result.description))
    return render(request, "build_model/results.html", {'results': results})
