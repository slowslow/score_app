from django.apps import AppConfig


class BuildModelConfig(AppConfig):
    name = 'build_model'
