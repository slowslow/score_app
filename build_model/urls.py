from django.conf.urls import url

from . import views

app_name = 'build_model'
urlpatterns = [
    url(r'^$', views.all, name='all'),
    url(r'^(?P<model_id>[0-9]+)/info$', views.info, name='info'),
    url(r'^(?P<dataset_id>[0-9]+)$', views.build, name="build"),
    url(r'^(?P<model_id>[0-9]+)/delete_model',
        views.delete_model, name='delete_model'),
    url(r'^(?P<model_id>[0-9]+)/test$', views.test, name='test'),
    url(r'^results/$', views.results, name='results'),
]
