### Install

docker-compose run web  python3 manage.py migrate

docker-compose up


# SETUP

### dev environment

```
sudo apt install python3-pip
mkvitualenv score_app
pip3 install -r requirements.txt
sudo -u postgres psql
postgres=# CREATE DATABASE score;
CREATE DATABASE
postgres=# CREATE USER score_user WITH password 'YOUR_PASSWORD';
CREATE ROLE
postgres=# GRANT ALL ON DATABASE score TO score_user;
GRANT
postgres=#
python3 manage.py migrate
python3 manage.py createsuperuser
```
