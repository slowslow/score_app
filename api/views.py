import json
import pandas as pd
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from dataset.models import Dataset


@login_required
def get_field(request, dataset_id, field_name):
    try:
        if not request.user.is_superuser:
            dataset = Dataset.objects.get(pk=dataset_id, user=request.user)
        else:
            dataset = Dataset.objects.get(pk=dataset_id)
    except Exception:
        return HttpResponseRedirect(reverse('dataset:upload'))

    desc = json.loads(dataset.description)
    if desc[field_name]["type"] == "number":
        data_frame = pd.read_csv(dataset.file.url)
        values = []
        for k, v in data_frame[field_name].to_dict().items():
            values.append([k, v]) 
        desc[field_name]['values'] = values
        desc[field_name]['history'] = ['x', 'y']        

    return JsonResponse(desc[field_name], safe=False)
