from django.conf.urls import url

from . import views

app_name = 'api'
urlpatterns = [
    url(r'^(?P<dataset_id>[0-9]+)/(?P<field_name>[\w\-]+)$', views.get_field, name='get_field'),
]
