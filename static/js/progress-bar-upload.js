$(function () {

  $(".js-upload-photos").click(function () {
    $("#fileupload").click();
  });

  $("#fileupload").fileupload({
    dataType: 'json',
    sequentialUploads: true,

    start: function (e) {
      $("#modal-progress").modal("show");
     },

    stop: function (e) {
      $("#modal-progress").modal("hide");
    },

    progress: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      var strProgress = progress + "%";
      $(".progress-bar").css({"width": strProgress});
      $(".progress-bar").text(strProgress);
    },

    done: function (e, data) {
      if (data.result.is_valid) {        
        $("#file-table tbody").prepend(
          "<tr><td><a href='" + data.result.url + "'>" + data.result.name + "</a></td></tr>"
        )
        var edit_link = "/dataset/" + data.result.id + "/edit"
        window.location.replace(edit_link);
      } else {
        $('#alert_placeholder').prepend(
          '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><span>' + data.result.error + '</span></div>'
          )
        $(".progress-bar").css({"width": "0%"});
        $(".progress-bar").text("0%");
      }
    }

  });

});
