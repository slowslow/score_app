function createSelect(parent_element, choices, name, select_type){

    //Create and append select list
    var selectList = document.createElement("select");
    selectList.setAttribute('class', 'custom-select mr-sm-2')
    selectList.setAttribute('name', name + select_type)
    parent_element.appendChild(selectList);

    //Create and append the options
    for (var i = 0; i < choices.length; i++) {
        var option = document.createElement("option");
        if (i == "choices"){
            option.id = name + "_choices"
        }
        option.value = choices[i];
        option.text = choices[i];
        selectList.appendChild(option);
    }
}

function createLabel(parent_element, name, lclass){
    var input_label = document.createElement("label");
    input_label.setAttribute("class", lclass);
    input_label.innerHTML = name;
    parent_element.appendChild(input_label);
}

function createDiv(parent_element, pclass){
    var col_div = document.createElement("div");
    col_div.setAttribute('class', pclass);
    parent_element.appendChild(col_div);
    return col_div;
}

function addParam(form_element, label, name, value){

    name_div = createDiv(form_element, 'form-group row')
    createLabel(name_div, name, 'col-sm-2 col-form-label');
    name_col = createDiv(name_div, 'col-sm-2')
    var form_input = document.createElement("input");
    form_input.setAttribute('class', 'form-control')
    form_input.readOnly = true;
    form_input.setAttribute('type',"text");
    form_input.setAttribute('name',name);
    form_input.setAttribute('id',name);
    form_input.setAttribute('value', value);
    name_col.appendChild(form_input);
    
    
    select_div = createDiv(form_element, 'form-group row')
    createLabel(select_div, "type", 'col-sm-2 col-form-label');
    select_col = createDiv(select_div, 'col-sm-2')
    var type_choices = ["int","choice","string", "boolean"];
    createSelect(select_col, type_choices, name, '_type');
    
    req_div = createDiv(form_element, 'form-group row')
    createLabel(req_div, "required", 'col-sm-2 col-form-label');
    req_col = createDiv(req_div, 'col-sm-2')
    var require_choices = ["optional", "required"];
    createSelect(req_col, require_choices, name, '_required');
}

$( ".custom-select" ).click(function() {
  var e = document.getElementById("inlineFormCustomSelect");
  var classifier_name = e.options[e.selectedIndex].innerHTML;
  var endpoint = "/model/get_classifier/?name=" + classifier_name + '&type=exist';
  $.ajax({
    method: "GET",
    url: endpoint,
    success: function(data){
        var form_element = document.getElementById('classifier-input')
        var container_form = document.getElementsByClassName('form-container')
        if (container_form.length != 0){
            $( ".form-container" ).remove();
        }
        var form_div = createDiv(form_element, 'form-container')
        document.getElementById("submit-button").style.visibility = "visible";
        for (var i in data) {
            param_card = createDiv(form_div, 'card')
            card_body = createDiv(param_card, 'card-body')
            name_div = createDiv(card_body, 'form-group row')
            createLabel(name_div, i, 'col-sm-2 col-form-label');
            name_col = createDiv(name_div, 'col-sm-2')
            if (data[i]["type"] == "number") {
                var form_input = document.createElement("input");
                form_input.setAttribute("class", "form-control")
                form_input.setAttribute("name", i)
                form_input.setAttribute("type", "number")
                form_input.setAttribute('value', data[i]["default"]);
                name_col.appendChild(form_input);
            } else if (data[i]["type"] == "string") {
                var form_input = document.createElement("input");
                form_input.setAttribute("class", "form-control")
                form_input.setAttribute("name", i)
                form_input.setAttribute("type", "string")
                form_input.setAttribute('value', data[i]["default"]);
                name_col.appendChild(form_input);
            } else if (data[i]["type"] == "choice") {
                var require_choices = data[i]["choice"];
                createSelect(req_col, require_choices, name, '_required');
            }
            //addParam(card_body, i, i, data[i] )
        }
    }
  });
});
