var re = /dataset/gi;
var str = window.location.pathname;
var newstr = str.replace(re, 'api');
var endpoint = newstr
$.ajax({
    method: "GET",
    url: endpoint,
    success: function(data){
   		labels = data.history
   		values = data.values
	   		var canvas = document.createElement('canvas');
	   		canvas.id = "myChart";
			canvas.width = 200;
			canvas.height = 200;
			$('#chartContainer').append(canvas);	
	    if (data.type == "select") {
	    	setChart('bar')
	    } else {
	    	setChart('line')
    	}
    },
    error: function(error_data){
        console.log("error")
        console.log(error_data)
    }
})

function setChart(chart_type){
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: chart_type,
	    data: {
	        labels: labels,
	        datasets: [{
	            label: 'field',
	            data: values,
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255,99,132,1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
	});
}